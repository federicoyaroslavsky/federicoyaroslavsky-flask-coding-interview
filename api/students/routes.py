from typing import Optional

from flask_openapi3 import APIBlueprint
from pydantic import BaseModel
from sqlalchemy import select
from datetime import datetime
from fastapi import HTTPException

from api.students.models import Students
from database import db

students_app = APIBlueprint("students_app", __name__)


class StudentSchema(BaseModel):
    id: int
    user_id: int
    enrolment_date: datetime

    class Config:
        orm_mode = True

class StudentCreateSchema(BaseModel):
    user_id: int
    enrolment_date: datetime

class StudentUpdateSchema(BaseModel):
    user_id: Optional[int]
    enrolment_date: Optional[datetime]

class StudentList(BaseModel):
    students: list[StudentSchema]

class StudentResultSchema(BaseModel):
    status_code: int
    detail: str
    data: Optional[StudentSchema]

def get_student_by_id(session, id: int):
    student = session.query(Students).filter(Students.id == id).first()
    if not student:
        raise HTTPException(status_code=404, details="User not found")


@students_app.get("/students", responses={"200": StudentList})
def get_students():
    with db.session() as session:
        students_query = session.execute(select(Students)).scalars().all()
        students_list = [
            StudentSchema.from_orm(student).dict()
            for student
            in students_query
        ]
        return {"students": students_list}
    
@students_app.get("/students/{id: int}", responses={"200": StudentSchema})
def get_student_by_id(id: int):
    with db.session() as session:
        student = get_student_by_id(session, id)
        return student
    
@students_app.post("/students", responses={"201": StudentSchema})
def create_student(body: StudentCreateSchema):
    with db.session() as session:
        student = Students(**body.dict())
        session.add(student)
        session.commit()
        return student
    
@students_app.put("/students/{id: int}", responses={"200": StudentSchema})
def update_student(id: int, body: StudentUpdateSchema):
    with db.session() as session:
        student = get_student_by_id(session, id)
        for field, value in body.dict().items():
            setattr(student, field, value)
        session.commit()    
        return student
    
@students_app.delete("/students/{id: int}", responses={"200": StudentResultSchema})
def delete_student(id: int):
    with db.session() as session:
        student = get_student_by_id(session, id)
        session.delete(student)
        session.commit()
        return {"status_code": 200, "detail": "The student was deleted successfully"}
